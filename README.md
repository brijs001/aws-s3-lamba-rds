AWS Lambda XML processing
------------------------

This code helps to get started with AWS lambda using nodejs to process XML file and store in db.


1) Create a folder in S3 bucket where XML file is to be uploaded. Use sample xml file for testing

2) Create a RDS Mysql database, make it public, connect it using workbench and create table demodb( id, patientid, producerid, data)

3) Remove public access to mysql and add it to your default VPC using RDS console.

4) Create a Lambda function in Amazon console, upload the attached zip

5) Map Lambda Trigger to S3 bucket with Prefix as folder name and suffix and xml.

6) Upload the xml to bucket, see the lambda logs.

Note: You can edit the lambda code from AWS console once zip file is uploaded.