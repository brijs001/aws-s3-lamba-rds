// welcome to Lambda Demo Application

var mysql = require('mysql');
var AWS = require('aws-sdk');
var xml2js = require('xml2js');

var s3 = new AWS.S3();

//This handler will invok after receiving the event with username and password
exports.handler = function(event, context) {
    console.log('Received event:', JSON.stringify(event, null, 2));
    const bucket = event.Records[0].s3.bucket.name;
    const key = decodeURIComponent(event.Records[0].s3.object.key.replace(/\+/g, ' '));
    const params = {
        Bucket: bucket,
        Key: key
    };
    s3.getObject(params, function(err, data) {
        if (err) {
            console.log(err);
            const message = "Error getting object ${key} from bucket ${bucket}. Make sure they exist and your bucket is in the same region as this function.";
            console.log(message);
            callback(message);
        } else {
            if(data.ContentType === 'text/xml'){
                console.log("Xml file uploaded", data);
                //TODO add xml logic
                ProcessXML(data);
            }
        }
    });
    function ProcessXML(data){
        var parser = new xml2js.Parser();
        console.log("XMl File data is: ", data.Body.toString());
        parser.parseString(data.Body.toString(), function (err, result) {
            console.log("Processed JSON Data :", result);
            StoreInDb(result);
        });
    }
    function StoreInDb(insertData){
        var connection = mysql.createConnection({
            host      :  'demodb.test.us-east-1.rds.amazonaws.com' ,  // give your RDS endpoint  here
            user      :  'test' ,  // Enter your  MySQL username
            password  :  'test' ,  // Enter your  MySQL password
            database  :  'demodb'    // Enter your  MySQL database name.
        });
        connection.connect(function(err) {
            if (err) {
                console.error('error connecting: ' + err.stack);
                return;
            }
            //console.log('connected as id ' + connection.threadId);
        });
        var id = Math.random() * (4000 - 10) + 10;
        var patient_id = 1;
        var producer_id = 233;
        var data = insertData['patient']['data'][0];
        var sql = 'INSERT INTO patient (`id`,`patientid`, `producerid`, `data`) VALUES("'+id+'","'+patient_id+'","'+producer_id+'","'+data+'")'; // set query for inserting userame and password in database
        console.log("Sql query is: ", sql);
        connection.query(sql, function (error, results, fields) {
            if (error) {
                throw error;
            }
            console.log('Query Response: ', results);
        });
        connection.end();
    }
}